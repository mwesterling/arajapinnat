@extends('layouts.app')

@section('content')

<h1>All bars</h1>

@foreach ($bars as $bar)

<div>
  <a href="/bars/{{$bar->id}}">{{$bar->name}}</a>
</div>

@endforeach

@stop
