@extends('layouts.app')

@section('content')
<div class="row">
  <div class="col-md-6 col-md-offset-3">
<h1>{{$bar->name}}</h1>

<ul class="list-group">
@foreach ($bar->notes as $note)

<li class="list-group-item">{{$note->body}}</li>

@endforeach
</ul>

<h3>Lisää arvio</h3>
<form method="POST" action="/bars/{{ $bar->id }}/notes">
<div class="form-group">
<textarea name="body" class="form-control"></textarea>
</div>
<div class="form-group">
<input type="hidden" name="_token" value="{{ csrf_token() }}">
<button type="submit" class="btn btn-primary">Lähetä</button>
</div>
</form>

</div>
</div>
@stop
