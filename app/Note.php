<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Note extends Model
{

protected $fillable = ['body'];

    public function bar(){

      return $this->belongsTo(Bar::class);
    }
}
