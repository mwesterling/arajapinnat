<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bar extends Model
{
    public function notes() {
      return $this->hasMany(Note::class);
    }
}
