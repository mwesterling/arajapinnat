<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Bar;
use App\Http\Requests;
use DB;

class BarsController extends Controller
{
    public function index() {

      $bars = Bar::all();

      return view('bars.index', compact('bars'));
    }

    public function show(Bar $bar) {

      return view('bars.show', compact('bar'));
    }


}
