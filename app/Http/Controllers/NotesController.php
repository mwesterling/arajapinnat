<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Note;
use App\Bar;

class NotesController extends Controller
{
    public function store(Request $request, Bar $bar) {

      $note = new Note;
      $note->body = $request->body;
      $bar->notes()->save($note);

      return back();

    }
}
