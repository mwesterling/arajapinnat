<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

/*Route::get('/', function () {
  $people = [];
    return view('welcome', compact('people'));
});

Route::get('helloworld', function() {
    return 'Hello World!';

});

Route::get('helloworld/second', function() {
    return 'Hello World! part 2';

});

Route::get('about', function() {
  return view('pages.about');
});*/

Route::get('/', 'PageController@home');
Route::get('about', 'PageController@about');

Route::get('bars', 'BarsController@index');
Route::get('bars/{bar}', 'BarsController@show');

Route::post('bars/{bar}/notes', 'NotesController@store');
